package bts.strings;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String s = "One";
        String s1 = "One";
        String s2 = new String("One");
        /*System.out.println(s.equals(s1));
        System.out.println(s1.equals(s2));
        System.out.println(s.charAt(1));
        System.out.println(s1.endsWith("."));
        System.out.println(s1.endsWith("One"));
        */

        String longString = "Hello my friend";
        System.out.println(longString.indexOf("o", 8));
        String[] split = longString.split(" ", 2);
        System.out.println(Arrays.toString(split));
        String substring = longString.substring(6, 8);
        System.out.println(substring);
        String longString2 = "          Hello my friend           w";
        System.out.println(longString2);
        System.out.println(longString2.trim());
    }
}

class SB {
    public static void main(String[] args) {
        StringBuilder builder = new StringBuilder();
        builder.append("Hello")
                .append(" ");
        String n = "H";
        for (int i = 0; i< 1000000; i++) {
            builder.append(i)
                    .append("|");
            if (i % 100 == 0) {
                builder.append('\n');
            }
        }

        builder.insert(0, "<3")
                .append("World!!!");

        String s = builder.toString();
        System.out.println(s.length());
    }
}
