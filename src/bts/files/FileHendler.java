package bts.files;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class FileHendler {
    public static void main(String[] args) {
        reader();
//        try (FileInputStream fileInputStream = new FileInputStream("test.txt");
//             FileOutputStream out = new FileOutputStream("test.txt")) {
//            char[] string = new char[fileInputStream.available()];
//            char count = 0;
//            while (fileInputStream.available() > 0) {
//                char read = (char) fileInputStream.read();
//                string[count] = read;
//                count++;
//            }
//            fileInputStream.close();
//            System.out.println(Arrays.toString(string));
//            String newLine = "Hello from program";
//
//            char[] chars = newLine.toCharArray();
//            for (int i = 0; i < chars.length; i++) {
//                out.write(chars[i]);
//            }
//            out.flush();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    public static void reader() {
        char[] chars = new char[10];
        char[] chars2 = new char[10];
        try (FileReader fileReader = new FileReader("test.txt")){
            System.out.println(fileReader.getEncoding());
            fileReader.read(chars);
            fileReader.read(chars2);
            System.out.println(Arrays.toString(chars));
            System.out.println(Arrays.toString(chars2));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {

        }
    }
}
