package bts.lambd;

import bts.schedular.TimeTable;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntBinaryOperator;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Lambds {
    private static String sufix = "ssss";
    static int count = 0;

    public static void main(String[] args) {

        List<String> strings = new ArrayList<String>() {{
            add("one");
            add("two");
            add("Hello");
        }};
        Function<String, TimeTable> aNew = TimeTable::new;
        Consumer<String> stringConsumer = s -> System.out.println(s);
        Function<String, Integer> powFunction = Integer::valueOf;
        Predicate<String> predicate = s -> {
            boolean contains = s.contains("0");
            boolean matches = s.matches("[0-9]*");
            return matches || contains;
        };

        strings.forEach(stringConsumer);
        List<Integer> collect = strings
                .stream()
                .filter(predicate)
                .map(powFunction)
                .collect(Collectors.toList());
    }

    public int sum(int x, int y) {
        return x + y;
    }
}
