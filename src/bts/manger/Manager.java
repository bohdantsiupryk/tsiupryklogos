package bts.manger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;

public class Manager {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Манагер покупок!!!!!!");
        List<Purchase> purchase = new ArrayList<Purchase>() {{
            add(new Purchase("a", 1));
            add(new Purchase("a", 2));
            add(new Purchase("a", 3));
            add(new Purchase("b", 4));
            add(new Purchase("a", 5));
            add(new Purchase("a", 6));
            add(new Purchase("a", 7));
            add(new Purchase("z", 3));
            add(new Purchase("z", 2));
            add(new Purchase("z", 11));
            add(new Purchase("z", 2332));
            add(new Purchase("a", 1212));
            add(new Purchase("a", 11));
            add(new Purchase("a", 43));
            add(new Purchase("c", 1));
            add(new Purchase("a", 6));
            add(new Purchase("a", 7));
            add(new Purchase("j", 8));
            add(new Purchase("a", 8));
            add(new Purchase("a", 345));
        }};
        while (true) {
            System.out.println("Порядок: \n 1. по імені \n 2. звичайний \n 3. кількість");
            int menuPoint = scanner.nextInt();
            switch (menuPoint) {
                case 1:
                    TreeSet<Purchase> purchases = new TreeSet<>(purchase);
                    printAllList(purchases);
                    break;
                case 2:
                    printAllList(purchase);
                    break;
                case 3:
                    Collections.sort(purchase, new PurchaseCountComparator());
                    printAllList(purchase);
                    break;
                default:
                    System.exit(0);
            }
        }
    }

    private static List<Purchase> getPurchase() {
        ArrayList<Purchase> purchases = new ArrayList<>();
        while (true) {
            Purchase purchase = readPurchase();
            purchases.add(purchase);
            System.out.println("Додати ще покупку? (y/n)");
            String cont = scanner.next();
            if (cont.equals("n")) {
                return purchases;
            }
        }
    }

    private static void printAllList(Collection<Purchase> collection) {
        int counter = 1;
        for (Purchase p : collection) {
            System.out.println(counter + ": " +p.getPrintText());
            counter++;
        }
    }

    private static Purchase readPurchase() {
        System.out.print("Назва - ");
        String name = scanner.next();
        System.out.print("кількість - ");
        int count = scanner.nextInt();
        return new Purchase(name, count);
    }
}

class PurchaseCountComparator implements Comparator<Purchase> {
    @Override
    public int compare(Purchase o1, Purchase o2) {
        return o1.getCount() - o2.getCount();
    }
}

class TablePriceComparator implements Comparator<Table>{

    @Override
    public int compare(Table o1, Table o2) {
        return Float.compare(o1.getPrice(), o1.getPrice());
    }
}

class Table {
    private long id;
    private String name;
    private float price;
    private int count;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}