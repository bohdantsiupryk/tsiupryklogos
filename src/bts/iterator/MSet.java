package bts.iterator;

import java.util.*;

public class MSet {
    public static void main(String[] args) {
        ArrayList<Test> list = new ArrayList<Test>() {{
            add(new Test(3));
            add(new Test(5));
            add(new Test(2));
            add(new Test(3));
            add(new Test(1));
            add(new Test(4));
        }};
        System.out.println(list);
        Set<Test> set = new TreeSet<>(list);
        System.out.println(set);
    }
}

class Test implements Comparable<Test> {
    private int id;

    public Test(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Test{" +
                "id=" + id +
                '}';
    }

    @Override
    public int compareTo(Test o) {
        return this.id - o.id;
    }
}