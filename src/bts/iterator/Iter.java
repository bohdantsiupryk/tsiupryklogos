package bts.iterator;

import java.util.ArrayList;
import java.util.Iterator;

public class Iter {
    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<String>() {{
            add("one");
            add("seven");
            add("six");
            add("five");
            add("four");
        }};

        Iterator<String> iterator = strings.iterator();
        while (true) {
            String next = iterator.next();

            System.out.println(next);

            if (next.equals("five")) {
                iterator.remove();
            }
        }
    }
}
