package bts.iterator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class Compare {
    public static void main(String[] args) {
        ArrayList<Human> humans = new ArrayList<Human>() {{
            add(new Human("Pavlo", 12, "LongLong@Email.com"));
            add(new Human("Andru", 13, "LongL222ong@Ema13il.com"));
            add(new Human("Oleg", 22, "Lon22g2Long@Email13.com"));
            add(new Human("Ivan", 16, "Long22212Long@Email.com"));
            add(new Human("Oleg", 10, "LongL2211ong@Email.com"));
            add(new Human("Petro", 11, "LongL2121ong@Ema12il.com"));
        }};

        Collections.sort(humans, new HumanEmailLengthComparator());
        System.out.println(humans);
    }
}

class Human implements Comparable<Human> {
    private String name;
    private int age;
    private String email;

    public Human(String name, int age, String email) {
        this.name = name;
        this.age = age;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int compareTo(Human o) {
        int i = this.name.compareTo(o.name);
        if (i == 0) {
            return this.age - o.getAge();
        }
        return i;
    }

    @Override
    public String toString() {
        return "\nHuman{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                '}';
    }
}

class HumanAgeComparator implements Comparator<Human> {
    @Override
    public int compare(Human o1, Human o2) {
        return o1.getAge() - o2.getAge();
    }
}

class HumanEmailLengthComparator implements Comparator<Human> {
    @Override
    public int compare(Human o1, Human o2) {
        return o1.getEmail().length() - o2.getEmail().length();
    }
}