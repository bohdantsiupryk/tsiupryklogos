package bts.lists;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class MyArrayList<T> implements OurList<T> {
    private final static int DEFAULT_CAPACITY = 16;

    private Object data[];

    private int lastElementNumber = 0;
    private int currentCapacity = 0;

    public MyArrayList() {
        this.data = new Object[DEFAULT_CAPACITY];
        this.currentCapacity = DEFAULT_CAPACITY;
    }

    public MyArrayList(int capacity) {
        this.data = new Object[capacity];
        this.currentCapacity = capacity;
    }

    @Override
    public void add(T value) {
        checkCapacityBeforeAdd(1);
        if (data.length == 0) {
            this.data[lastElementNumber] = value;
            return;
        }
        lastElementNumber++;
        this.data[lastElementNumber] = value;
    }

    @Override
    public void add(T value, int index) {
        checkIndex(index);
        checkCapacityBeforeAdd(1);
        if (index > lastElementNumber + 1) {
            add(value);
            return;
        } else if (index <= lastElementNumber) {
            Object[] firstPart = Arrays.copyOfRange(data, 0, index);
            Object[] secondPart = Arrays.copyOfRange(data, index, lastElementNumber + 1);
            Object[] newData = new Object[currentCapacity];
            for (int i = 0; i < newData.length; i++) {
                if (i < firstPart.length) {
                    newData[i] = firstPart[i];
                } else if (i == index) {
                    newData[i] = value;
                } else {
                    newData[i] = secondPart[i - firstPart.length - 1];
                }
            }
            this.data = newData;
            lastElementNumber++;
        }
     }

    @Override
    public void addAll(List<T> list) {
        checkCapacityBeforeAdd(list.size());
        for (T element : list) {
            add(element);
        }
    }

    @Override
    public T get(int index) {
        checkIndex(index);
        return (T) data[index];
    }

    @Override
    public void set(T value, int index) {
        checkIndex(index);
        checkCapacityBeforeAdd(1);
        if (index > lastElementNumber) {
            add(value);
            return;
        }
        this.data[index] = value;
    }

    @Override
    public T remove(int index) {
        checkIndex(index);

        Object tmp = this.data[index];
        if (index == lastElementNumber) {
            this.data[lastElementNumber] = null;
        } else if (index < lastElementNumber) {
            Object[] secondPart = Arrays.copyOfRange(this.data, index + 1, lastElementNumber + 1);
            for (int i = index; i <= lastElementNumber; i++) {
                this.data[i] = secondPart[i - index];
            }
            this.data[lastElementNumber] = null;
        } else {
            throw new NoSuchElementException();
        }
        checkCapacityAfterRemove();
        lastElementNumber--;
        return (T) tmp;
    }

    private void checkIndex(int index) {
        if (index < 0) {
            throw new NegativeIndexException();
        }
    }

    @Override
    public T remove(T t) {
        for (int i = 0; i < data.length; i++) {
            if (t.equals(data[i])) {
                checkCapacityAfterRemove();
                return remove(i);
            }
        }
        throw new NoSuchElementException();
    }

    @Override
    public int size() {
        return lastElementNumber + 1;
    }

    @Override
    public boolean isEmpty() {
        return lastElementNumber == 0;
    }

    private void checkCapacityBeforeAdd(int numberOfElements) {
        if (currentCapacity * 0.9 < size() + numberOfElements) {
            this.data = Arrays.copyOf(data, currentCapacity + (currentCapacity >> 1));
        }
    }

    private void checkCapacityAfterRemove() {
        int cap = this.currentCapacity - (this.currentCapacity >> 2);
        if (size() < cap) {
            this.data = Arrays.copyOf(data, cap);
        }
    }
}
