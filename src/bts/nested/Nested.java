package bts.nested;

import java.util.ArrayList;
import java.util.Comparator;

public class Nested {
    private NewOne newField;

    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<>();
        strings.sort(new Comparator<String>() { //anonim
            @Override
            public int compare(String o1, String o2) {
                return 0;
            }
        });

        class Car { //local

        }
    }

    public class NewOne { //inner

    }

    public enum RGBColors {
        BLUE, GREED, YELLOW
    }

    public static class SMYCColors {
        public static final String DARK_BLUE = "#6641";
    }
}
