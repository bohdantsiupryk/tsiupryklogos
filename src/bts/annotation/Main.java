package bts.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class Main {
    @MyAnnotation
    private int field;

    public static void main(String[] args) throws NoSuchFieldException {
        Main main = new Main();
        Class<? extends Main> clazz = main.getClass();
        Field field = clazz.getDeclaredField("field");
        field.setAccessible(true);
        MyAnnotation declaredAnnotation = field.getDeclaredAnnotation(MyAnnotation.class);
        System.out.println(declaredAnnotation.message());
        System.out.println(declaredAnnotation.value());
    }
}
