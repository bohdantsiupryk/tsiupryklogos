package bts.solid;

public class OCprinc {
    public static void main(String[] args) {
        Human human = new Student();
        human.sayHelloTo("Bob");
    }
}

class Human {
    private String name;

    public void sayHelloTo(String name) {
        System.out.println("Hello " + name);
    }
    //get & set
}

class Student extends Human {
    private int age;

}