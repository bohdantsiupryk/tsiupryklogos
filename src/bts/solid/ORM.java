package bts.solid;

import java.util.UUID;

public class ORM {
    public static void main(String[] args) {
        String file = new String();

    }

}

class ConnectionOperator {
    public Connection createConnection() {
        Connection connection = new Connection();
        return connection;
    }

    public void killConnection(Connection conn) {
        System.out.println("kill - " + conn.getId());
    }
}

class DBOperator {
    public void saveData(String data, Connection conn) {
        System.out.println("Save " + data + " by connection " + conn.getId());
    }

    public void removeData(String data, Connection conn) {
        System.out.println("Remove " + data + " by connection " + conn.getId());
    }
}

class DBOperatorWithEncrypt extends DBOperator {
    public void saveDataWithEncrypt(String data, Connection conn) {
        String s = data.toUpperCase();
        super.saveData(s, conn);
    }
}

class Connection {
    private String id;

    public Connection() {
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }
}

