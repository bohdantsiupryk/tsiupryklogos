package bts.inhabitans;

public abstract class Animal {
    private String name;
    private int age;

    public Animal() {
    }

    public void voice() {
        System.out.println("My voice is my name " + name);
    }

    public void move() {
        System.out.println("Move");
    }

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
