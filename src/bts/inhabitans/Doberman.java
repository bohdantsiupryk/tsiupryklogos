package bts.inhabitans;

public class Doberman extends Dog {
    private String color;

    public Doberman(String name, int age, int tailLength, String color) {
        super(name, age, tailLength);
        this.color = color;
    }
}
