package bts.inhabitans;

public class Fish extends Animal {
    private int numberPlavnik;

    public Fish(String name, int age, int numberPlavnik) {
        super(name, age);
        this.numberPlavnik = numberPlavnik;
    }

    @Override
    public void move() {
        System.out.println("Swimming");
    }
}
