package bts.inhabitans;

public class Dog extends Animal {
    private int tailLength;

    public Dog(String name, int age, int tailLength) {
        super(name, age);
        this.tailLength = tailLength;
    }

    @Override
    public void voice() {
        System.out.println("Gav");
    }

    @Override
    public void move() {
        System.out.println("Running");
    }
}