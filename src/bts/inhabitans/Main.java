package bts.inhabitans;

public class Main {
    public static void main(String[] args) {
        Dog barbos = new Dog("Barbos", 12, 60);
        Fish dori = new Fish("Dori", 1, 4);
        Fish dori2 = new Fish("Dori2", 1, 4);
        Doberman doberman = new Doberman("Vasya", 6, 10, "Black");
        Animal[] animals = new Animal[] {doberman, barbos, dori, dori2};
        sayVoice(animals);
    }

    public static void sayVoice(Animal[] animals) {
        for (Animal animal : animals) {
            animal.voice();
        }
    }
}
