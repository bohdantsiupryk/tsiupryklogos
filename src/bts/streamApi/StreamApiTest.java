package bts.streamApi;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamApiTest {
    public static void main(String[] args) {
        List<List<String>> strings = new ArrayList<List<String>>() {{
            add(Arrays.asList("1", "-2"));
            add(Arrays.asList("55", "3", "55", "3"));
            add(Arrays.asList("5", "6", "6"));
            add(Arrays.asList("45", "12"));
            add(Arrays.asList("2", "544"));
        }};
        Stream<String> filteredStream = strings.stream()
                .flatMap(list -> list.stream())
                .filter(s -> s.matches("-?[0-9]*"));
        filteredStream
                .forEach(s -> System.out.println(s));
        Integer reduce = strings.stream()
                .flatMap(list -> list.stream())
                .map(s -> Integer.valueOf(s))
                .reduce(Integer.valueOf(0), (i1, i2) -> i1 * i2);
    }
}
