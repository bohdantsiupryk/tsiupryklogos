package bts.streamApi;

import bts.exceptions.Human;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.*;

public abstract class MyInteface {
    static int i = 0;

    public static void main(String[] args) {
        List<Human> list = new ArrayList<>();
        list.stream()
                .sorted(comparing(Human::getAge)
                        .thenComparing(Human::getName)
                        .thenComparing(Human::getEmail, reverseOrder()))
                .collect(Collectors.toList());
        new Human();
        int i = 9;
        Integer[] i2 = new Integer[2];
        System.exit(0);
    }

    public void handle() {

    }
}
