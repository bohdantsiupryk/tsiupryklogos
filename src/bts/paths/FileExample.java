package bts.paths;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.function.BiPredicate;
import java.util.stream.Stream;

public class FileExample {
    public static void main(String[] args) throws IOException {
        BiPredicate<Path, BasicFileAttributes> biPredicate = new BiPredicate<Path, BasicFileAttributes>() {
            @Override
            public boolean test(Path path, BasicFileAttributes basicFileAttributes) {
                return path.toString().endsWith("java");
            }
        };
        ArrayList<String> strings = new ArrayList<String>() {{
            add("two");
            add("two");
            add("two");
            add("two");
            add("two");
            add("two");
            add("two");
            add("two");
        }};

        Path path = Paths.get("test.txt");
        Files.copy(path, Paths.get("test2.txt"), StandardCopyOption.REPLACE_EXISTING);
        Files.move(Paths.get("test2.txt"), Paths.get("out\\test3.txt"), StandardCopyOption.REPLACE_EXISTING);
        Stream<Path> pathStream = Files.find(Paths.get("."), 4, biPredicate, FileVisitOption.FOLLOW_LINKS);
        pathStream.forEach(o -> System.out.println(o));
        Files.write(path, strings, StandardOpenOption.APPEND);
    }
}

class MyVisit extends SimpleFileVisitor<Path> {
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        System.out.println(file);
        return super.visitFile(file, attrs);
    }
}
