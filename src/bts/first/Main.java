package bts.first;

public class Main {
    static int number; //- оголошення
    static int number2 = 10; //- оголошення і ініціалізація
    static int sum = number + number2;

    static {
        sum = number - number2;
    }

    public static void main(String[] args) {
        Modificators modificators = new Modificators();
        modificators.anInt = 0;

        int methodVar = 7;

        if (true) {
            methodVar++;
            number2 ++;
            int ifValue = 9;
        }

        int ifValue;
    }

    public static void charConvert(int firstValue,
                                    int secondValue) {
        number--;
    }
}