package bts.first;

import java.util.Scanner;

public class CodeTime {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] numberArray = new int[1000];
        System.out.println("вводьте числа, для закінчення" +
                "введіть 0");

        int count = 0;
        while (scanner.hasNext()) {
            int valueFromConsole = scanner.nextInt();
            numberArray[count] = valueFromConsole;
            count++;
            if (valueFromConsole == 0) {
                break;
            }
        }
        System.out.println("Кількіст цифр = " + count);
        System.out.println("Сума = " + sumArray(numberArray, count));
    }

    public static Integer sumArray(int[] array, int size) {
        if (array.length < size) {
            System.out.println("Неправильні значення розміру");
            return null;
        }

        int sum = 0;
        for (int i = 0; i < size; i++) {
            sum += array[i];
        }
        return sum;
    }
}
