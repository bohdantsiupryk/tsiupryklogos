package bts.first;

public class Wrappers {
    static Integer value1 = new Integer(70);

    public static void main(String[] args) {
        Integer integer = null;
        int i = Integer.parseInt("123");
        double v = integer.doubleValue();

        System.out.println(i);
    }

    public static void boxing() {
        Long longValue = 87472136L;
        Byte byteValue = 12;
        Character charValue = 'y';
        char unChar = charValue;

        longValue = null;

        byte b = longValue.byteValue();

        if (longValue == null) {

        }
    }
}
