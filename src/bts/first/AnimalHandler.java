package bts.first;

public class AnimalHandler {
    public static void main(String[] args) {
        Animal cat = new Animal();
        Animal dog = new Animal("Rex", 9);
        Animal fish = new Animal("Fish");
        fish.setName("Dori");
        System.out.println(cat);
        System.out.println(dog);
        System.out.println(fish);
        fish.setAge(1);
        System.out.println(fish);
        fish.growUpOn(4);
        System.out.println(fish);
        bornAnimal();

        Animal.sendToEmail(fish);
    }

    public static Animal bornAnimal() {
        Animal dog = new Animal("Doberman");
        dog.sayName();
        dog.setName("Rex");
        dog.sayName();
        return dog;
    }
}
