package bts.first;

public class Animal {
    private String name;
    private String poroda;
    private int age;

    public Animal() {
        this.name = "Vasya";
        this.age = 8;
    }

    public Animal(String name, int age) {
        this.age = age;
        this.name = name;
    }

    public Animal(String poroda) {
        this.poroda = poroda;
        this.name = "Щенок";
        this.age = 0;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void sayName() {
        System.out.println("My name is - " + this.name);
    }

    public void growUpOn(int year) {
        this.age += year;
    }

    public static void sendToEmail(Animal animal) {
        System.out.println(animal.name + " " + animal.age);
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", age=" + age + '\'' +
                ", poroda=" + poroda +
                '}';
    }
}
