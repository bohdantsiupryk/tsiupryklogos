package bts.map;

public class FOP {
    private long id;
    private long bankId;

    public FOP(long id, long bankId) {
        this.id = id;
        this.bankId = bankId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBankId() {
        return bankId;
    }

    public void setBankId(long bankId) {
        this.bankId = bankId;
    }

    @Override
    public String toString() {
        return "FOP{" +
                "id=" + id +
                ", bankId=" + bankId +
                '}';
    }
}
