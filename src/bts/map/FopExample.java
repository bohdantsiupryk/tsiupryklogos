package bts.map;

import java.util.*;

public class FopExample {
    private Map<Person, List<FOP>> personFOPMap = new HashMap<>();

    public static void main(String[] args) {
        FopExample fopExample = new FopExample();
        fopExample.fillMap();
        System.out.println(fopExample.getFOPByTaxNumber(1313L, 2L));
    }

    public FOP getFOPByTaxNumber(Long taxNumber, Long bankId) {
        HashMap<Long, List<FOP>> longFOPHashMap = new HashMap<>();
        for (Map.Entry<Person, List<FOP>> entry : personFOPMap.entrySet()) {
            long number = entry.getKey().getTaxNumber();
            List<FOP> value = entry.getValue();
            longFOPHashMap.put(number, value);
        }

        List<FOP> fops = longFOPHashMap.get(taxNumber);
        for (FOP f : fops) {
            if (f.getBankId() == bankId) {
                return f;
            }
        }

        throw new NoSuchElementException();
    }

    private void fillMap() {
        Person oleg = new Person(9992L, "Oleg", 20);
        Person ihor = new Person(1313L, "Ihor", 16);
        Person petro = new Person(4347L, "Petro", 66);
        Person ivan = new Person(4334, "Ivan", 31);

        FOP fop1 = new FOP(1L, 1L);
        FOP fop3 = new FOP(2L, 2L);
        FOP fop2 = new FOP(3L, 3L);
        FOP fop = new FOP(4L, 4L);
        ArrayList<FOP> fops = new ArrayList<>();
        fops.add(fop1);
        fops.add(fop2);
        fops.add(fop3);
        personFOPMap.put(oleg, fops);
        personFOPMap.put(ihor, fops);
        personFOPMap.put(petro, fops);
        personFOPMap.put(ivan, fops);
    }
}
