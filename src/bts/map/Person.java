package bts.map;

import java.io.Serializable;
import java.util.Objects;

public class Person implements Serializable, Comparable<Person> {
    private long taxNumber;
    private String name;
    private int age;

    public Person() {
    }

    public Person(long taxNumber, String name, int age) {
        this.taxNumber = taxNumber;
        this.name = name;
        this.age = age;
    }

    public Person(long taxNumber) {
        this.taxNumber = taxNumber;
    }

    public long getTaxNumber() {
        return taxNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "taxNumber=" + taxNumber +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Person o) {
        return 0;
    }
}