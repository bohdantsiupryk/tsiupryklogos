package bts.map;

import java.util.HashMap;
import java.util.Map;

public class MapExample {
    public static void main(String[] args) {
        Person vasya = new Person(1298479263156L, "Vasya", 20);
        Person olay = new Person(1124124156L, "Olya", 20);
        Person pavlo = new Person(129133356L, "Pavlo", 20);

        HashMap<Long, Person> personHashMap = new HashMap<>();
        personHashMap.put(vasya.getTaxNumber(), vasya);
        personHashMap.put(olay.getTaxNumber(), olay);
        personHashMap.put(pavlo.getTaxNumber(), pavlo);

        personHashMap.replace(pavlo.getTaxNumber(), olay);
        System.out.println(personHashMap.get(olay.getTaxNumber()));


//        findByTaxNumber(1298479263156L, personHashMap);
    }

    public static void findByTaxNumber(long taxNumber, HashMap<Long, Person> personHashMap) {
        Person person = personHashMap.get(taxNumber);
        System.out.println(person);
    }
}


