package bts.reflection;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) throws IOException {
        String canonicalPath = new File("C:\\Users\\Bohdan\\IdeaProjects\\tsiupryklogos\\src\\bts").getCanonicalPath();
        Path path = Paths.get(canonicalPath);
        Iterator<Path> iterator = path.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
