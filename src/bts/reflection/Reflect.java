package bts.reflection;

import bts.map.FopExample;
import bts.map.Person;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;

public class Reflect {
    private FopExample fopExample;


    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException {
        Class<Person> personClass = (Class<Person>) Class.forName("bts.map.Person");
        personClass.getDeclaredAnnotations();
        Constructor<?>[] constructors = personClass.getConstructors();
        Person person = new Person();

        for (Constructor f : constructors) {
            if (f.getParameterCount() == 1) {
                person = (Person) f.newInstance(Long.valueOf(2992L));
                System.out.println(person);
            }
        }

        Field[] fields = person.getClass().getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);
            if (f.getName().equals("taxNumber")) {
                f.set(person, Long.valueOf(1000000L));
            }
        }
        System.out.println(person);
    }
}
