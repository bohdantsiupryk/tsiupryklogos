package bts.enums;

import java.util.Arrays;

public enum Signal {
    RED(192,"red", "Dark red", "RED") {
        @Override
        public void action() {
            System.out.println("STOP");
        }
    },
    GREEN(192, "green", "GREEN") {
        @Override
        public void action() {
            System.out.println("GO");
        }
    },
    YELLOW(192,"yellow") {
        @Override
        public void action() {
            System.out.println("READY");
        }
    },
    NONE(52);

    private String[] color;
    private int ip;

    public void action() {
        System.out.println("DEFAULT");
    }

    Signal(int ipAdress, String... color) {
        this.color = color;
        this.ip = ipAdress;
    }

    public static Signal findByString(String str) {
        Signal[] values = Signal.values();
        for (Signal v : values) {
            boolean compare = compare(str, v);
            if (compare) {
                return v;
            }
        }
        return Signal.NONE;
    }

    private static boolean compare(String str, Signal signal) {
        String[] color = signal.getColor();
        for (String col : color) {
            boolean equals = col.equals(str);
            if (equals) {
                return true;
            }
        }
        return false;
    }

    public String[] getColor() {
        return color;
    }
}
