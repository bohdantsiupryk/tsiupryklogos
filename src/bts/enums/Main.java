package bts.enums;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = "";
        while (!s.equals("stop")) {
            s = scanner.nextLine();
            Signal signal = Signal.findByString(s);
            signal.action();
        }
    }

    private enum Signal1 {
        GO, STOP
    }
}
