package bts.optional;

import java.util.Optional;
import java.util.function.Consumer;

public class OptionalMain {
    public static void main(String[] args) throws ClassNotFoundException {
        handle("String");
        handle(null);
    }

    public static void handle(String s) throws ClassNotFoundException {
        String s3 = Optional.ofNullable(s)
                .orElseThrow(() -> new ClassNotFoundException());

        Optional<String> optionalString = Optional.ofNullable(s);
        optionalString.ifPresent(createConsumer());
        String defaultString = optionalString.orElse("Default String");
        String throwString = optionalString.orElseThrow(() -> new ClassNotFoundException());
        Optional<String> s2 = optionalString.map(string -> string.trim());

        if (optionalString.isPresent()) {
            String s1 = optionalString.get();
            String trim = s1.trim();
            String concat = trim.concat(".txt");
            System.out.println(concat);
        }
    }

    public static Consumer<String> createConsumer() {
        return string -> {
            String trim = string.trim();
            String concat = trim.concat(".txt");
            System.out.println(concat);
        };
    }
}
