package bts.memory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("Some@email.com");

        String email1 = "Some@email.com";
        String email2 = "Someemail.com";
        String email3 = "Some2@email.com";
        String email4 = "Some@emai2l.com";

        Matcher matcher = pattern.matcher(email1);
        Matcher matcher1 = pattern.matcher(email2);
        Matcher matcher2 = pattern.matcher(email3);
        Matcher matcher3 = pattern.matcher(email4);

        System.out.println(matcher.matches());
        System.out.println(matcher1.matches());
        System.out.println(matcher2.matches());
        System.out.println(matcher3.matches());

    }
}
