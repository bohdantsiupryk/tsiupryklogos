package bts.memory;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

public class Main {
    public static int cout = 0;
    public static LocalDateTime date;

    public static void main(String[] args) {
        int i = 1;
        int j = 2;
        int c = 0;
        new IntegerY(
                i,
                j,
                (i < j) ? (i) : (j),
                i < j ? i : j
        );
    }

    public static void exp() {
        StringBuilder builder = new StringBuilder();
    }
}

class IntegerY {
    private int a;
    private int b;
    private int c;
    private int d;

    public IntegerY(int a, int b, int c, int d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntegerY integerY = (IntegerY) o;
        return a == integerY.a &&
                b == integerY.b &&
                c == integerY.c &&
                d == integerY.d;
    }
}
