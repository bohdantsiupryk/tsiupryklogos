package bts;

import java.util.ArrayList;
import java.util.List;

public class Yeager {
    public static int id;
    public int age;

    public Yeager(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Yeager{" +
                "id=" + id +
                "age=" + age +
                '}';
    }
}
