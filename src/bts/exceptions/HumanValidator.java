package bts.exceptions;

import bts.exceptions.except.IncorrectEmailException;
import bts.exceptions.except.UnderAgeException;

public class HumanValidator {
    public static void validateHumanAge(Human human)
            throws UnderAgeException {
        if (human.getAge() < 18) {
            throw new UnderAgeException(human.getAge());
        }
    }

    public static void validateHumanEmail(Human human)
            throws IncorrectEmailException {
        if (!human.getEmail().contains("@")) {
            throw new IncorrectEmailException();
        }
    }
}
