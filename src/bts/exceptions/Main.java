package bts.exceptions;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            openSomeFile("Tru tu tu");
        } catch (MyException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("FINALLY");
        }
    }

    public static void openSomeFile(String fileName) throws IOException {
        throw new MyException("cannot open file" + fileName);
    }

    public static void openSomeFileWithRuntime() {
        throw new RuntimeException();
    }
}

class MyException extends IOException {
    public MyException(String message) {
        super("From my exception - " + message);
    }
}
