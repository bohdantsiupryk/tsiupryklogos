package bts.exceptions;

import bts.exceptions.except.IncorrectEmailException;
import bts.exceptions.except.UnderAgeException;

import java.util.Arrays;

/**
 * author Bohdan
 */
public class HumanMain {
    public static void main(String[] args) {
        Human[] humans = new Human[]{
                new Human("Vasya", "AAa@gg.com", 22),
                new Human("Olya", "AAa@gg.com", 13),
                new Human("Bodya", "AAagg.com", 44),
                new Human("Oleg", "", 11),
                new Human("Stepan", "AAa@gg.com", 6),
                new Human("Kolya", "AAa@gg.com", 66),
                null,
        };

        Human[] validHumans = new Human[humans.length];
        int count = 0;
        //For each human apply validation
        for (Human human : humans) {
            boolean valid = validateHuman(human);
            if (valid) {
                validHumans[count] = human;
                count++;
            }
        }
        Human[] humans1 = Arrays.copyOf(validHumans, count);
        System.out.println("Валідні Люди : \n" + Arrays.toString(humans1));
    }

    public static boolean validateHuman(Human human) {
        boolean valid = true;
        if (human == null) {
            return false;
        }

        try {
            HumanValidator.validateHumanAge(human);
        } catch (UnderAgeException e) {
            System.out.println("Шановний " + human.getName() +
                    " ваш вік замалий," + e.getMessage());
            valid = false;
        }

        try {
            HumanValidator.validateHumanEmail(human);
        } catch (IncorrectEmailException e) {
            System.out.println("Шановний " + human.getName() +
                    " ваш email некоректний");
            valid = false;
        }
        return valid;
    }
}
