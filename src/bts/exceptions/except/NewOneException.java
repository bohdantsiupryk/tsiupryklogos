package bts.exceptions.except;

public class NewOneException extends UnderAgeException {
    public NewOneException(int age) {
        super(age);
    }
}
