package bts.schedular;

public class TimeTable {
    private String task;

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public TimeTable(String task) {
        this.task = task;
    }
}
