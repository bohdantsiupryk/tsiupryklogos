package bts.schedular;

public enum Week {
    MONDAY("понеділок", 1, new TimeTable("Скопати город")),
    TUESDAY("вівторок", 2, new TimeTable("Скопати город у бабці")),
    WEDNESDAY("середа", 3, new TimeTable("Вчити Джава")),
    THURSDAY("сетвер", 4, new TimeTable("Вчити Яву")),
    FRIDAY("п'ятниця", 5, new TimeTable("Бухнути")),
    SATURDAY("субота", 6, new TimeTable("Відійти від бухіча")),
    SUNDAY("неділя", 7, new TimeTable("Йдемо до церкви"));

    private String dayName;
    private int dayNumber;
    private TimeTable timeTable;

    Week(String dayName, int dayNumber, TimeTable timeTable) {
        this.dayName = dayName;
        this.dayNumber = dayNumber;
        this.timeTable = timeTable;
    }

    public TimeTable getTimeTable() {
        return timeTable;
    }

    public void setTimeTable(TimeTable timeTable) {
        this.timeTable = timeTable;
    }

    public String getDayName() {
        return dayName;
    }
}
