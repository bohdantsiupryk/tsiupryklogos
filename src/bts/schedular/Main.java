package bts.schedular;

import javax.naming.NameNotFoundException;
import java.util.Scanner;

public class Main {
    private Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Main main = new Main();
        String dayFromScanner = main.readFromScanner("Введіть день тижня");
        Week day;
        try {
            day = main.convertFromString(dayFromScanner);
            main.printScheduler(day);
        } catch (NameNotFoundException e) {
            System.out.println("Введенно неправильний день");
        }
        main.nextTurn();
    }

    public void nextTurn() {
        System.out.println("Бажаєте продовжити....");
        String action = readFromScanner("(так/ніт)");
        if (Action.NO.getV().equals(action)) {
            System.exit(0);
        } else if (Action.YES.getV().equals(action)){
            main(new String[]{});
        }
        nextTurn();
    }

    public Week convertFromString(String str) throws NameNotFoundException {
        Week[] values = Week.values();
        for (Week day : values) {
            boolean equals = day.getDayName().equals(str);
            if (equals) {
                return day;
            }
        }
        throw new NameNotFoundException();
    }

    public String readFromScanner(String message) {
        System.out.println(message);
        return scanner.nextLine();
    }

    public void printScheduler(Week day) {
        String task = day.getTimeTable().getTask();
        System.out.println("Твоє завдання на " + day.getDayName() + '-' + task);
    }

    private enum Action {
        YES("так"),
        NO("ніт");

        private String v;

        Action(String v) {
            this.v = v;
        }

        public String getV() {
            return v;
        }
    }
}
