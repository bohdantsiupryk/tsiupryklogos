package bts.serializable;

import java.io.Serializable;

public class ExampleObject {
    private static final long serialVersionUID = 9L;
    private long id;
    private String text;
    private boolean include;

    public ExampleObject(long id, String text, boolean include) {
        this.id = id;
        this.text = text;
        this.include = include;
    }

    public ExampleObject() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isInclude() {
        return include;
    }

    public void setInclude(boolean include) {
        this.include = include;
    }

    @Override
    public String toString() {
        return "ExampleObject{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", include=" + include +
                '}';
    }
}
