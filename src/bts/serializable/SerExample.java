package bts.serializable;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class SerExample {
    public static void main(String[] args) {
        ExampleObject someText = new ExampleObject(1L, "Some text", false);
        serialize(ExampleObject.class);
    }

    public static <T> void serialize(T o) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("test.txt"))) {
            out.writeObject(o);
            System.out.println("Записано успішно");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T> T desialize(Class<T> clazz) {
        T result;
        try (FileInputStream fileInputStream = new FileInputStream("test.txt");
             ObjectInputStream in = new ObjectInputStream(fileInputStream)) {
            result = (T) in.readObject();
            return result;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (ClassCastException cs) {
            System.out.println("Десеріалізований об'єкт не типу " + clazz);
        }
        return null;
    }
}
