package bts.generic;

import java.util.List;

public interface UserService<T extends Account> {
    void addUser(User user);
    void addAccountToUser(User user, T account);
    List<User> getAllUsersSortedByName();
    List<T> getAllAccounts();
    double getTax(User user);
}
