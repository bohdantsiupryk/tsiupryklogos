package bts.generic;

import java.util.*;
import java.util.stream.Collectors;

public class UserServiceImpl<T extends Account>
        implements UserService<T> {

    private static final double TAX_PERCENT = 0.05;

    private Map<User, List<T>> usersMap = new HashMap();

    @Override
    public void addUser(User user) {
        ArrayList<T> accounts = new ArrayList<>();
        usersMap.put(user, accounts);
    }

    @Override
    public void addAccountToUser(User user, T account) {
        List<T> accs = usersMap.get(user);
        accs.add(account);
    }

    @Override
    public List<User> getAllUsersSortedByName() {
        Set<User> users = this.usersMap.keySet();
        return new ArrayList<>(users);
    }

    @Override
    public List<T> getAllAccounts() {
        List<T> collect = usersMap.values().stream()
                .flatMap(u -> u.stream())
                .collect(Collectors.toList());

        ArrayList<T> ts = new ArrayList<>();
        for (Collection c : usersMap.values()) {
            ts.addAll(c);
        }

        return ts;
    }

    @Override
    public double getTax(User user) {
        List<T> accounts = usersMap.get(user);

        Double result = accounts.stream()
                .map(a -> a.getCash() * TAX_PERCENT)
                .reduce(new Double(0), (x, y) -> x + y);

        Double tax = 0.0;
        for (T acc : accounts) {
            tax += acc.getCash() * TAX_PERCENT;
        }

        return tax;
    }
}
