package bts.generic;

public class Account<T> {
    private T id;
    private String accountName;
    private long cash;

    public Account(T id, String accountName) {
        this.id = id;
        this.accountName = accountName;
        this.cash = 10;
    }

    public long getCash() {
        return cash;
    }

    public void addCash(long value) {
        this.cash += value;
    }

    public T getId() {
        return id;
    }

    public void setId(T id) {
        this.id = id;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
