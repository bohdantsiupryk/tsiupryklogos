package bts.threds;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class MyThreads {
    public static void main(String[] args) throws InterruptedException {
        Runnable mTread = new MTread("first");
        Runnable mTread2 = new MTread("second");
        Runnable mTread3 = new MTread("second2");
        ExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.submit(mTread);
        executorService.submit(mTread2);
        executorService.submit(mTread3);

        executorService.shutdown();
    }

    public static void test() {
        synchronized(Object.class) {
            System.out.println("LOL");
        }
    }
}

class MTread implements Runnable {
    int i = 0;
    String name;

    public MTread(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        System.out.print(name);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            System.out.println("Шо хоч?");
        }
        System.out.println(" - " + i++);
    }
}
